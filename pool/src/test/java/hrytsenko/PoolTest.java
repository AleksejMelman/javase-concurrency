package hrytsenko;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PoolTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PoolTest.class);

    private static final int RESOURCES_MIN = 3;
    private static final int TASKS_NUM = 20;
    private static final int WORKERS_NUM = 5;

    @Test
    public void test() throws InterruptedException {
        AtomicLong counter = new AtomicLong();
        Pool<Connection> pool = new Pool<>(() -> {
            Connection connection = new Connection(counter.incrementAndGet());
            connection.open();
            return connection;
        }, RESOURCES_MIN);

        ExecutorService executor = Executors.newFixedThreadPool(WORKERS_NUM);
        IntStream.range(0, TASKS_NUM).forEach(i -> executor.execute(() -> {
            LOGGER.info("task {}: create", i);
            trySleep(200);

            Connection connection = pool.acquire();
            LOGGER.info("task {}: acquire {}", i, connection.getId());
            trySleep(200);

            LOGGER.info("task {}: release {}", i, connection.getId());
            pool.release(connection);
        }));

        executor.shutdown();
        executor.awaitTermination(60, TimeUnit.SECONDS);

        pool.close();
        LOGGER.info("total: {} conn", counter.get());
    }

    static class Connection implements AutoCloseable {
        private final long id;

        public Connection(long id) {
            this.id = id;
            LOGGER.info("conn {}: create", id);
        }

        public Object getId() {
            return id;
        }

        public void open() {
            LOGGER.info("conn {}: open", id);
        }

        @Override
        public void close() {
            LOGGER.info("conn {}: close", id);
        }
    }

    static void trySleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
        }
    }

}
