package hrytsenko;

import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Pool<T extends AutoCloseable> implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Pool.class);

    private Supplier<T> supplier;

    private Queue<T> resources;
    private volatile boolean closed;

    public Pool(Supplier<T> supplier, int size) {
        this.supplier = supplier;

        this.resources = new ArrayBlockingQueue<>(size);
    }

    public T acquire() {
        if (closed) {
            throw new IllegalStateException("Pool is closed.");
        }
        return Optional.ofNullable(resources.poll()).orElseGet(supplier);
    }

    public synchronized void release(T resource) {
        if (closed) {
            close(resource);
            return;
        }
        if (!resources.offer(resource)) {
            close(resource);
        }
    }

    @Override
    public synchronized void close() {
        if (closed) {
            return;
        }
        closed = true;
        while (!resources.isEmpty()) {
            Optional.ofNullable(resources.poll()).ifPresent(Pool::close);
        }
    }

    private static <T extends AutoCloseable> void close(T resource) {
        try {
            resource.close();
        } catch (Exception exception) {
            LOGGER.warn("Cannot close the resource {}.", resource);
        }
    }

}
