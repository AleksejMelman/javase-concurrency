package hrytsenko;

public class BlurTransform {

    private int[] src;
    private int[] dst;
    private int radius;

    public BlurTransform(int[] src, int[] dst, int radius) {
        this.src = src;
        this.dst = dst;
        this.radius = radius;
    }

    public void accept(int from, int to) {
        for (int i = from; i < to; ++i) {
            int r = 0;
            int g = 0;
            int b = 0;
            int windowFrom = Math.max(i - radius, 0);
            int windowTo = Math.min(i + radius, src.length - 1);
            for (int j = windowFrom; j <= windowTo; ++j) {
                r += (src[j] & 0x00ff0000) >> 16;
                g += (src[j] & 0x0000ff00) >> 8;
                b += (src[j] & 0x000000ff) >> 0;
            }
            int windowSize = windowTo - windowFrom + 1;
            dst[i] = ((r / windowSize) << 16) | ((g / windowSize) << 8) | ((b / windowSize) << 0);
        }
    }

}
