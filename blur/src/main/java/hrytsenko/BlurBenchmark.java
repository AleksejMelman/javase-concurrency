package hrytsenko;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class BlurBenchmark {

    private int[] src;
    private int[] dst;
    private BlurTransform transform;

    @Setup
    public void init() {
        src = new int[1920 * 1080];
        Arrays.fill(src, 0x00FFFFFF);
        dst = new int[src.length];
        transform = new BlurTransform(src, dst, 10);
    }

    @Benchmark
    public void sequential() {
        transform.accept(0, src.length);
    }

    @Benchmark
    public void parallel() {
        IntStream.range(0, src.length).parallel().forEach(i -> transform.accept(i, i + 1));
    }

    @Benchmark
    public void fork() {
        ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        pool.invoke(new TransformAction(transform::accept, 0, src.length, 10000));
    }

}
