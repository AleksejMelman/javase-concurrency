package hrytsenko;

import java.util.concurrent.RecursiveAction;
import java.util.function.BiConsumer;

public class TransformAction extends RecursiveAction {

    private static final long serialVersionUID = 1L;

    private BiConsumer<Integer, Integer> transform;
    private int from;
    private int to;
    private int threshold;

    public TransformAction(BiConsumer<Integer, Integer> transform, int from, int to, int threshold) {
        this.transform = transform;
        this.from = from;
        this.to = to;
        this.threshold = threshold;
    }

    @Override
    protected void compute() {
        if (to - from < threshold) {
            transform.accept(from, to);
            return;
        }

        int pivot = (to - from) / 2;
        invokeAll(new TransformAction(transform, from, from + pivot, threshold),
                new TransformAction(transform, from + pivot, to, threshold));
    }

}
